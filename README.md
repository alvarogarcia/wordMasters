# Word Masters
This is the final project I had to do in order to complete the following course:  
https://frontendmasters.com/courses/web-development-v3/

It's a recreation of the word game Wordle, a game that you can find in The New York Times.

The instructions are here:  
https://btholt.github.io/complete-intro-to-web-dev-v3/lessons/talking-to-servers/project

And you can find my result on:  
https://wordmasters.alvarogarcia.xyz
