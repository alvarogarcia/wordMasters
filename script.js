let nextRowIndex = 0
let nextGapIndex = 0
let wordOfTheDay
let gaps
const rows = document.querySelectorAll('.row')
const loadingIcon = document.querySelector('.loading-icon')

function getCurrentGaps() {
  return rows[nextRowIndex].querySelectorAll('.gap')
}

function isLetter(letter) {
  return /^[a-zA-Z]$/.test(letter)
}

function writeLetter(character) {
  if (nextGapIndex > 4) {
    return
  }

  gaps[nextGapIndex].innerText = character
  nextGapIndex++
}

function eraseLetter() {
  if (nextGapIndex === 0) {
    return
  }

  nextGapIndex--
  gaps[nextGapIndex].innerText = ''
}

function setIsLoading(isLoading) {
  loadingIcon.classList.toggle('invisible', !isLoading)
}

function getWrittenWord() {
  const writtenWord = [...gaps]
    .map((gap) => gap.innerText)
    .join('')
  return writtenWord
}

function isTheRightWordWritten() {
  const writtenWord = getWrittenWord().toLocaleLowerCase()
  const isTheRightWordWritten = writtenWord === wordOfTheDay
  return isTheRightWordWritten
}

function isLetterInTheWordOfTheDay(letter, startFromIndex) {
  const splittedWordOfTheDay = wordOfTheDay.split('')
  const lettersWhereLookFor = splittedWordOfTheDay.slice(startFromIndex)
  return lettersWhereLookFor.includes(letter)
}

function isLetterInTheWordOfTheDayIndex(letter, index) {
  const splittedWordOfTheDay = wordOfTheDay.split('')
  const wordOfTheDayLetter = splittedWordOfTheDay[index]
  return letter === wordOfTheDayLetter
}

function styleGaps() {
  gaps.forEach((gap, index) => {
    const letter = gap.innerText.toLocaleLowerCase()
    if (isLetterInTheWordOfTheDayIndex(letter, index)) {
      return gap.classList.add('gap-correct')
    }

    if (isLetterInTheWordOfTheDay(letter, index)) {
      return gap.classList.add('gap-close')
    }

    gap.classList.add('gap-incorrect')
  })
}

async function onWordSubmitting() {
  const writtenWord = getWrittenWord()
  const isValidWord = await checkIfWordIsValid(writtenWord)
  if (!isValidWord) {
    gaps.forEach((gap) => gap.classList.add('gap-invalid'))
    setTimeout(() => 
      gaps.forEach((gap) => gap.classList.remove('gap-invalid')), 
      500
    )
    return
  }

  styleGaps()

  if (isTheRightWordWritten()) {
    alert('You win!')
    document
      .querySelector('.header')
      .classList
      .add('rainbow-animation')
    return
  }

  if (nextRowIndex > 4) {
    alert(`You lose, the word was ${wordOfTheDay}`)
    return
  }

  nextRowIndex++
  nextGapIndex = 0
  gaps = getCurrentGaps()
}

async function loadWordOfTheDay() {
  const URL = 'https://words.dev-apis.com/word-of-the-day'
  const response = await fetch(URL)
  const { word } = await response.json()
  wordOfTheDay = word
}

async function checkIfWordIsValid(word) {
  const URL = 'https://words.dev-apis.com/validate-word'
  const response = await fetch(
    URL, 
    { 
      method: 'POST', 
      body: JSON.stringify({ word }) 
    }
  )
  const { validWord } = await response.json()
  return validWord
}

async function init() {
  gaps = getCurrentGaps()
  await loadWordOfTheDay()
  setIsLoading(false)

  document
  .querySelector('.board')
  .addEventListener('keydown', async function(event) {
    const character = event.key
    
    if (isLetter(character)) {
      writeLetter(character)
    }

    if (character === 'Backspace') {
      eraseLetter()
    }

    if (character === 'Enter' && nextGapIndex > 4) {
      setIsLoading(true)
      await onWordSubmitting()
      setIsLoading(false)
    }
  })
}

init()
